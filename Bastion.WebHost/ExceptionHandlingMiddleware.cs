﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using System;
using System.Net;
using System.Threading.Tasks;

namespace Bastion.WebHost
{
    /// <summary>
    /// The exception handling middleware.
    /// </summary>
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingMiddleware"/> class.
        /// </summary>
        /// <param name="next">The request delegate.</param>
        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }


        /// <summary>
        /// Invokes the <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The HTTP context.</param>
        /// <returns>Execution task.</returns>
        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            _logger.LogError(exception, "Exception in processing " + context?.Request?.ToShortString());

            var innerException = GetInnerException(exception);

            var result = JsonConvert.SerializeObject(new
            {
                code = innerException?.HResult,
                error = innerException?.Message,
                traceId = context?.TraceIdentifier,
                stackTrace = exception?.StackTrace,
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return context.Response.WriteAsync(result);
        }

        private Exception GetInnerException(Exception exception)
        {
            return (exception?.InnerException is null) ? exception : GetInnerException(exception.InnerException);
        }
    }
}
