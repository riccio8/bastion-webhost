﻿namespace Bastion.WebHost.Responses
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Paging response.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagingResponse<T>
    {
        /// <summary>
        /// Gets or sets page nubmer.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets count item per page.
        /// </summary>
        public int PerPage { get; set; }

        /// <summary>
        /// Gets or sets total pages.
        /// </summary>
        public int Pages { get; set; }

        /// <summary>
        /// Gets or sets data array objects.
        /// </summary>
        public T[] Data { get; set; }

        public static PagingResponse<T> GetPageResponse(IEnumerable<T> elements, int perPage, int page)
        {
            PagingResponse<T> pagingResponse = new PagingResponse<T>()
            {
                PerPage = perPage,
                Page = page,
                Pages = elements.Count() / perPage,
                Data = elements.Skip((page - 1) * perPage).Take(perPage).ToArray()
            };
            if (elements.Count() % perPage > 0)
            {
                pagingResponse.Pages++;
            }
            return pagingResponse;
        }
    }
}
