﻿namespace Bastion.WebHost.Transformers
{
    using System.Text.RegularExpressions;

    using Microsoft.AspNetCore.Routing;

    public class LowerCamelCaseTransformer : IOutboundParameterTransformer
    {
        private static readonly string pattern = @"(?<=\w)(?=[A-Z])";

        public string TransformOutbound(object value)
        {
            if (value == null)
            {
                return null;
            }

            var result = Regex.Replace(value.ToString(), pattern, string.Empty, RegexOptions.None);

            return result.Substring(0, 1).ToLower() + result.Substring(1);
        }
    }
}
