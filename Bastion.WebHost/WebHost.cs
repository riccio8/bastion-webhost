﻿namespace Bastion.WebHost
{
    using System;
    using System.IO;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.SwaggerGen;

    /// <summary>
    /// Common web host class.
    /// </summary>
    public static class WebHost
    {
        /// <summary>
        /// Gets Application name.
        /// </summary>
        public static string AppName { get; private set; }

        /// <summary>
        /// The custom action that may be invoked by the underlaying service.
        /// </summary>
        public static Action<IServiceCollection> ServiceAction { get; private set; }

        /// <summary>
        /// The custom action that the underlaying service may invoke on the application builder.
        /// </summary>
        public static Action<IApplicationBuilder> ApplicationAction { get; private set; }

        /// <summary>
        /// The custom action that the underlaying service may invoke to configure the swagger.
        /// </summary>
        public static Action<SwaggerGenOptions> SwaggerAction { get; private set; }

        /// <summary>
        /// The services.
        /// </summary>
        public static IServiceProvider Services { get; private set; }

        /// <summary>
        /// Starts the web host.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        /// <param name="appName">The application name.</param>
        /// <param name="serviceAction">Custom service action.</param>
        /// <param name="applicationAction">Custom application action.</param>
        /// <remarks>This method has predefined Startup class for configuring the web service. The action delegates allow to add additional customizations.</remarks>
        public static void Start(   string[] args,
                                    string appName,
                                    Action<IServiceCollection> serviceAction = null,
                                    Action<IApplicationBuilder> applicationAction = null,
                                    Action<SwaggerGenOptions> swaggerAction = null)
        {
            AppName = appName;
            ServiceAction = serviceAction;
            ApplicationAction = applicationAction;
            SwaggerAction = swaggerAction;
            using (IHost host = BuildWebHost<Startup>(args))
            {
                Services = host.Services;
                host.Run();
            }
        }

        /// <summary>
        /// Starts the web host.
        /// </summary>
        /// <typeparam name="TStartup">The Startup class.</typeparam>
        /// <param name="args">Program arguments.</param>
        /// <remarks>This method allows to fully customize the web service according to your needs.</remarks>
        public static void Start<TStartup>(string[] args = null) where TStartup : class
        {
            using (IHost host = BuildWebHost<TStartup>(args))
            {
                Services = host.Services;
                host.Run();
            }
        }

        /// <summary>
        /// Builds and configures web host.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        /// <returns>Configured web host instance.</returns>
        private static IHost BuildWebHost<TStartup>(string[] args) where TStartup : class
        {
            return new HostBuilder().ConfigureWebHost(builder =>
            {
                //.UseKestrel(opt => opt.AddServerHeader = false)
                builder.UseContentRoot(Directory.GetCurrentDirectory());
                builder.ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    config.AddEnvironmentVariables();
                });
                builder.ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddEventSourceLogger();
                });
                builder.UseStartup<TStartup>();
            }).Build();
        }
    }
}
