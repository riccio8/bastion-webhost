namespace Bastion.WebHost
{
    using System;
    using System.IO;
    using System.Reflection;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Serialization;
    using Swashbuckle.AspNetCore.Filters;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using Swashbuckle.AspNetCore.SwaggerUI;

    using Bastion.WebHost.Transformers;
    using Microsoft.OpenApi.Models;

    /// <summary>
    /// Provides web-service startup configuration.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="env">The hosting environment.</param>
        /// <param name="configuration">Application configuration instance.</param>
        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            this.Environment = env;
            this.Configuration = configuration;
        }

        /// <summary>
        /// Gets hosting environment.
        /// </summary>
        public IWebHostEnvironment Environment { get; }

        /// <summary>
        /// Gets application configuration instance.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Collection of services descriptors.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddMvc(opt => opt.Conventions.Add(new RouteTokenTransformerConvention(new LowerCamelCaseTransformer())))
                .AddApplicationPart(Assembly.GetEntryAssembly())
                .AddControllersAsServices()
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.Converters.Add(new StringEnumConverter());
                    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            services.AddSwaggerGen(SwaggerGenerator);
            //services.AddSwaggerExamplesFromAssemblyOf();

            WebHost.ServiceAction?.Invoke(services);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Requests builder.</param>
        /// <param name="env">Web hosting environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware(typeof(ExceptionHandlingMiddleware));
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", WebHost.AppName);
                c.RoutePrefix = string.Empty;
                c.DocExpansion(DocExpansion.None);
            });

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();

            WebHost.ApplicationAction?.Invoke(app);
        }

        private void SwaggerGenerator(SwaggerGenOptions options)
        {
            options.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = WebHost.AppName,
                    Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Name = "Hedgehogs Team",
                        Email = "hedgehogs_team@hedgehogs.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Hedgehogs LICENSE",
                        Url = new Uri("https://hedgehogs.com/license")
                    }
                });

            options.DescribeAllEnumsAsStrings();
            options.EnableAnnotations();
            options.ExampleFilters();

            // Set the comments path for the Swagger JSON and UI.
            string xmlFile = $"{Assembly.GetEntryAssembly().GetName().Name}.xml";
            string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);

            WebHost.SwaggerAction?.Invoke(options);
        }
    }
}
