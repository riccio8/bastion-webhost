﻿namespace Bastion.WebHost.Hubs
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading.Channels;
    using Microsoft.AspNetCore.SignalR;
    using Newtonsoft.Json;

    public class DefaultHub : Hub
    {
        private  readonly ConcurrentDictionary<string, Sender> senders = new ConcurrentDictionary<string, Sender>();
        private const char Splitter = '-';

        public DefaultHub()
        {
        }

        public ChannelReader<string> Subscribe(string channelName)
        {
            if (!senders.TryGetValue(channelName, out Sender sender))
            {
                sender = new Sender();
                senders.TryAdd(channelName, sender);
            }

            if (channelName.Contains(Splitter))
            {
                string[] splitName = channelName.Split(Splitter);
                if (!senders.TryGetValue(splitName[0], out Sender commonSender))
                {
                    commonSender = new Sender();
                    senders.TryAdd(splitName[0], commonSender);
                }
                commonSender.Subscribe(splitName[1]);
            }
            else
            {
                sender.Subscribe();
            }
            return sender.Observable().AsChannelReader(10);
        }

        public void OnSubscribe(Type type, Action<string> action)
        {
            string channelName = type.Name;
            if (!senders.TryGetValue(channelName, out Sender sender))
            {
                senders.TryAdd(channelName, sender = new Sender());
            }
            sender.OnSubscribe += action;
        }

        public void Send(object msg, string extraName = null)
        {
            string msgString = JsonConvert.SerializeObject(msg);
            string channelName = GetChannelName(msg.GetType(), extraName);
            if (!senders.TryGetValue(channelName, out Sender sender))
            {
                senders.TryAdd(channelName, sender = new Sender());
            }

            sender.Send(msgString);
        }

        public void SendToChannel(object msg, string channelName)
        {
            string msgString = JsonConvert.SerializeObject(msg);
            if (!senders.TryGetValue(channelName, out Sender sender))
            {
                senders.TryAdd(channelName, sender = new Sender());
            }

            sender.Send(msgString);
        }

        private static string GetChannelName(Type type, string extraName)
        {
            string channelName = type.Name;
            if (!string.IsNullOrWhiteSpace(extraName))
            {
                channelName = $"{channelName}-{extraName}";
            }

            return channelName;
        }
    }
}