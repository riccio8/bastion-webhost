﻿namespace Bastion.WebHost.Hubs
{
    using System;
    using System.Reactive.Subjects;

    public class Sender
    {
        private readonly Subject<string> _subject = new Subject<string>();
        public event Action<string> OnSubscribe;

        public Sender()
        {
        }

        public IObservable<string> Observable()
        {
            return _subject;
        }

        public void Send(string msg)
        {
            _subject.OnNext(msg);
        }

        public void Subscribe(string extraName = null)
        {
            OnSubscribe?.Invoke(extraName);
        }
    }
}
