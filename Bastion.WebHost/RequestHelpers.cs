using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace Bastion.WebHost
{
    public static class RequestHelpers
    {
        public static string ToShortString(this HttpRequest request)
        {
            var encodedUrl = request?.GetEncodedUrl();
            return $"{request?.HttpContext?.TraceIdentifier}:{request?.Method} {encodedUrl}";
        }
    }
}